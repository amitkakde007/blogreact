import React, {Component} from 'react'
import {Button} from '@material-ui/core'

import '../JumbotronPanel/JumbotronPanel.css'
import logo from '../../assests/header_image.jpg'

class JumbotronPanel extends Component{
    render(){
        return(
            <div className='jumbotron__panel'>
                <div className='jumbotron__header'>
                    <div>Read,</div>
                    <div>Write,</div>
                    <div className='highlight__connect' style={{color:'#ffa500'}}>Connect...</div>
                    <div className='writing__button'><Button  style={{backgroundColor:'#ffa500',borderRadius:'50px'}}>Start blogging...</Button></div>
                </div>
                <div className='header__image'>
                    <img src={logo} alt="jumbotron_logo"></img>
                </div> 
            </div>
        )
    }
}

export default JumbotronPanel