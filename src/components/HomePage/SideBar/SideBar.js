import React from 'react'

import './SideBar.css'

function SideBarUpdate(props) {
    return (
        <div className='sidebar__updates'>
            <div className='sidebar__header'>Latest updates...</div>
            <div onClick={()=>console.log('clicked on post 1')} className='topic_card'>
                <small>Self</small>
                <span>Four Signs a Person Is Secretly Unhappy with Their Life</span>
            </div>
            <div onClick={()=>console.log('clicked on post 2')} className='topic_card'>
                <small>Data Science</small>
                <span>Do Not Use Print For Debugging In Python Anymore</span>
            </div>
            <div onClick={()=>console.log('clicked on post 3')} className='topic_card'>
                <small>Data Science</small>
                <span>Do Not Use Print For Debugging In Python Anymore</span>
            </div>
            <div onClick={()=>console.log('clicked on post 4')} className='topic_card'>
                <small>Data Science</small>
                <span>Do Not Use Print For Debugging In Python Anymore</span>
            </div>
            <div onClick={()=>console.log('show more')} className='sidebar__footer'>Show more...</div>
        </div>)
}

export function SuggestionTags() {
    return (
        <div>
            <span>Discover...</span>
            <div className='sidebar__content'>
                <div className='topic_name'>Data Science</div>
                <div className='topic_name'>Photography</div>
                <div className='topic_name'>Food</div>
                <div className='topic_name'>Travel</div>
                <div className='topic_name'>Fashion</div>
                <div className='topic_name'>Automobile</div>
            </div>
            <div style={{ color: 'lightgray', marginTop: '10px',  marginBottom:'50px' , borderBottom: '2px solid', width: '100%'}} />
        </div>
    )
}

export default SideBarUpdate;