import React from 'react'
import { useHistory } from 'react-router'

import './BlogShortsCard.css'
import avtar from '../../../../assests/user.png'

export default function BlogShortCard({blogData}) {

    const history = useHistory();
    return (
        <div className='blogshort__container' onClick={()=>history.push(`blog/${blogData.id}/`)}>
            <div className='blogshort__card'>
                <div className='blogshort__owner'>
                    <img src={avtar} alt='user_pic' />
                    <span>{blogData.author.fullname === '' ? 'Anonymous' : blogData.author.fullname}</span>
                </div>
                <div className='blogshort__title'>
                    <span>{blogData.title}</span>
                </div>
                <div className='blogshort__subtitle'>
                    <small>{blogData.content.length >= 20 ? `${blogData.content.substr(0, 50)}...` : blogData.content}</small>
                </div>
                <div className='blogshort__data'>
                    <small>{blogData.last_modified}</small>
                </div>
            </div>
            <div className='blogshort__img'>
                <img src={blogData.image} alt='blog_image'></img>
            </div>
        </div>
    )
}