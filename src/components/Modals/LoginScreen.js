import React from 'react'
import GoogleLogin from 'react-google-login'

export function LoginScreen({props}){
    return(
        <React.Fragment>
            <GoogleLogin
                clientId={props.clientId}
                onSuccess={props.responseGoogle}
                onFailure={props.errorGoogle}
                cookiePolicy={'single_host_origin'}
                render={renderProps => (
                    <div className='google__signin' style={{ cursor: 'pointer' }} onClick={renderProps.onClick}>
                        <img src="https://img.icons8.com/color/24/000000/google-logo.png" alt='google-logo' />Sign in with Google
                    </div>
                )}
            />
            <div className='email__signin' style={{ cursor: 'pointer' }} onClick={()=>props.setToggle('emailSignIn')}>
                <img src="https://img.icons8.com/ios-glyphs/25/000000/new-post.png" alt='email-logo' />Sign in with Email
            </div>
            <div className='sign__up' style={{ cursor: 'pointer' }}>
                No account ?  
                <span style={{ color: 'green', fontWeight: 'bold', marginLeft:'5px'}} onClick={() => props.setToggle('signUp')}>Create One</span>
            </div>
        </React.Fragment>
    )
}