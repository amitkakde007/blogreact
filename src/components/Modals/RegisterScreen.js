import React from 'react'


import './RegisterScreen.css'

export function RegisterScreen({ props }) {
    return (
        <React.Fragment>
            <div className='register__content'>
                <div className='firstname__field'>
                    <img src="https://img.icons8.com/material-rounded/30/000000/name.png" alt='first-name' />
                    <div>First Name</div>
                    <input placeholder='&nbsp;Enter your first name' />
                </div>
                <div className='lastname__field'>
                    <img src="https://img.icons8.com/material-rounded/30/000000/name.png" alt='last-name' /><div>Last Name</div><input placeholder='&nbsp;Enter your last name' />
                </div>
                <div className='emailaddress__field'>
                    <img src="https://img.icons8.com/ios-glyphs/25/000000/new-post.png" alt='email-icon' /><div>Email</div>
                    <input placeholder='&nbsp;Enter your email' />
                </div>
                <div className='password__field1' >
                    <img src="https://img.icons8.com/ios-glyphs/30/000000/password--v1.png" alt='password-icon' /><div>Password</div>
                    <input placeholder='&nbsp;Enter your password' type='password' />
                </div>
                <div className='password__field2' >
                    <img src="https://img.icons8.com/ios-glyphs/30/000000/password--v1.png" alt='password-icon' /><div>Re-enter Password</div>
                    <input placeholder='&nbsp;Enter your password again' type='password' />
                </div>
                <div className='register__button'>Submit</div>
            </div>
            <div className='sign__up' style={{ cursor: 'pointer' }}>
                Already got an account ?
                <span style={{ color: 'green', fontWeight: 'bold',marginLeft:'5px'}} onClick={() => props.setToggle('default')}>Sign In</span>
            </div>
        </React.Fragment>
    )
}