import React from 'react'

import './EmailScreen.css'

export function EmailScreen(){
    return(
        <React.Fragment>
        <div className='login__content'>
            <div className='email__field'>
                <div><img style={{marginRight:'13px'}} src="https://img.icons8.com/ios-glyphs/25/000000/new-post.png" alt='email-icon'/>Email</div>
                <input placeholder='&nbsp;Enter your email'/>
            </div>
            <div className='password__field' >
                <div><img style={{marginRight:'10px'}} src="https://img.icons8.com/ios-glyphs/30/000000/password--v1.png" alt='password-icon'/>Password</div>
                <input placeholder='&nbsp;Enter your password' type='password'/> <i class="bi bi-eye-slash" id="togglePassword"></i>
            </div>
            <div className='login__button'>Submit</div>
        </div>
        </React.Fragment>
    )
}