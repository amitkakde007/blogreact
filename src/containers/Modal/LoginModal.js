import React, { useState } from 'react'
import Modal from 'react-modal'

import './LoginModal.css'
import axiosInstance from '../../AxiosInstance'
import { stateUpdate } from '../../StateProvider'
import { LoginScreen } from '../../components/Modals/LoginScreen'
import { EmailScreen } from '../../components/Modals/EmailScreen'
import { RegisterScreen } from '../../components/Modals/RegisterScreen'
import { useHistory } from 'react-router-dom'

export default function LoginModal({show}) {

    const loginUrl = 'authenticate/google/login'
    const clientId = '638453212656-c00rj142qfocqhhr67p9as3asqhqojtn.apps.googleusercontent.com'
    const setUser = stateUpdate()
    const history = useHistory()

    const [showModal, setModal] = useState(show)
    const [toggleScreen,setToggle] = useState('default')

    const closeModal = () => {
        setModal(false)
        history.push('/')
    }

    const responseGoogle = (response) => {

        axiosInstance.post(loginUrl, { auth_token: response.tokenObj.id_token })
            .then((resp) => {setUser(resp.data);localStorage.setItem('isLoggedIn',true)})
            .then(() => {closeModal();history.push('/')})
            .catch(err => console.error(err))
    }
    const errorGoogle = response => console.error(response)

    const ModalBox = ()=>{
        let contentWindow;
            switch (toggleScreen) {
                case 'emailSignIn':
                   contentWindow = <EmailScreen props={{setToggle}}/>
                    break;
                case 'signUp':
                    contentWindow = <RegisterScreen props={{setToggle}}/>
                    break;
                default:
                    contentWindow = <LoginScreen props={{clientId,responseGoogle,errorGoogle,setToggle}}/>;
                    break;
            }
        return(
        <Modal
            isOpen={showModal}
            className='modal__box'
            overlayClassName='modal__overlay'
            appElement={document.getElementById('root')}>
            <div className='modal__header'>
                <div className='close__modal' >
                    <div onClick={()=>{closeModal();setToggle('default')}} style={{ color: 'gray', cursor: 'pointer', fontSize: 'xx-large' }}>&times;</div>
                </div>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <h2>Join us by...</h2>
                </div>
            </div>
            <div className='modal__content'>
                {contentWindow}
            </div>
            <div className='modal__footer'>
                <small>Click “Sign Up” to agree to with our Terms of Service and acknowledge that the Privacy Policy applies to you.</small>
            </div>
        </Modal>)}
    return (
        <React.Fragment>
            <ModalBox/>
        </React.Fragment>
    )
}