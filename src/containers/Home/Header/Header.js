import React, { Component } from 'react'
import logo from '../../../assests/blog_logo.png';
import {withRouter} from 'react-router'
import {NavLink} from 'react-router-dom'

import './Header.css'
import axiosInstance from '../../../AxiosInstance';
import { StateContext, StateUpdateContext } from '../../../StateProvider';

class Header extends Component {
    constructor({show}) {
        super(show)
        this.state = {
            show: show,
        }
    }
    navStyleContent={
        content:{
            color: 'white',
            textDecoration:'None'
        }
    }

    loggedUser =({setUser})=> {

        const logOut =()=>{
            axiosInstance.post('authenticate/logout/')
            .then(resp=>{setUser('');alert(resp.data.message);localStorage.removeItem('isLoggedIn');localStorage.removeItem('access')})
            .catch(err=>console.error(err))
        }
        return(
            <div className='user__options' style={{float:'left'}}>
                <img className='drop__btn' alt='user-pic' src="https://img.icons8.com/ios-filled/38/ffffff/user.png" />
                <div className="dropdown__content">
                    <li onClick={()=>console.log('account settings')}>Account Settings</li>
                    <li onClick={()=>logOut()}>Logout</li>
                </div>
            </div>
        )
    }
    render() {
        return (
            <React.Fragment>
                <div className='header__bar'>
                    <div className='header__logo' style={{cursor:'pointer'}} onClick={()=>this.props.history.push('/')}>
                        <img style={{ objectFit: 'contain', width: '150px' }} src={logo} alt="logo" />
                    </div>
                    <StateContext>{
                        loggedInUser=>
                        <StateUpdateContext>
                            {setUser=>
                            <div className='header__options'>
                                <li>
                                    <NavLink to='blog/create/new' style={this.navStyleContent.content}>{!loggedInUser? 'Write':<img alt='write-blog' src="https://img.icons8.com/ios-glyphs/30/ffffff/multi-edit.png"/>}
                                    </NavLink>
                                </li>
                                <li><NavLink to='/membership' style={this.navStyleContent.content}>{!loggedInUser? 'Membership':<img alt='notify-user' src="https://img.icons8.com/material-rounded/30/ffffff/appointment-reminders.png"/>}</NavLink></li>
                                <li  onClick={!loggedInUser? this.showModal:null}><NavLink to='/login' style={this.navStyleContent.content}> {loggedInUser? `Hi, ${loggedInUser.fullname.split(' ')[0]}`: 'Sign In'}</NavLink></li>
                                <li> <NavLink to='/' style={this.navStyleContent.content}>{!loggedInUser? 'Get Started':<this.loggedUser setUser={setUser}/>}
                                    </NavLink></li>
                            </div>
                        }
                    </StateUpdateContext>
                    }</StateContext>
                </div>
            </React.Fragment>
        )
    }
}

export default withRouter(Header);