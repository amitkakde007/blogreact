import React, { useEffect } from 'react'
import Cookie from 'universal-cookie'

import './HomePage.css'
import SideBar from '../Home/SideBar/SideBar'
import BlogList from './Blogs/BlogList'

import { authAxios } from '../../AxiosInstance'
import {stateData,stateUpdate} from '../../StateProvider'

function HomePage(){

    const cookie = new Cookie()
    const loggedInUser = stateData()
    const setUser = stateUpdate()

    const checkUser = ()=>{
    if (localStorage.getItem('isLoggedIn')==='true' && loggedInUser===''){
      authAxios.get(`authenticate/verify-user?token=${cookie.get('access')}`)
      .then(resp=>{setUser(resp.data)})
      .catch(err=>console.error(err))
      }}

    useEffect(()=>checkUser(),[loggedInUser])

    return(
        <React.Fragment>
            <div className='main__content'>
                <div className='blog__list'>                
                    <BlogList user={loggedInUser}/>
                </div>
                <div className='sidebar__list'>
                    <SideBar user={loggedInUser}/>
                </div>
            </div>
        </React.Fragment>
    )   
}

export default HomePage