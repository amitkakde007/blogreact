import React,{Component} from 'react'
import {withRouter} from 'react-router-dom'
import axiosInstance from '../../../AxiosInstance';

import BlogDetailCard from '../../../components/HomePage/Blog/BlogDetail/BlogDetailCard';
class BlogDetail extends Component{
    constructor(props){
        super(props);
        this.state={
            id:props.match.params.id,
            blog:{}
        }
    }
    fetchData =()=>{
        axiosInstance.get(`blog/blog-viewset/${this.state.id}/`)
        .then(resp=>this.setState({blog:resp.data}))
        .catch(err=>console.log(err))
    }
    componentDidMount(){
        this.fetchData()
    }
    render(){
        console.log(this.state.blog)
        return(
            <BlogDetailCard blog={this.state.blog}/>
        )
    }
}

export default withRouter(BlogDetail);