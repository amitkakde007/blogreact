import React, { Component } from 'react'
import axiosInstance from '../../../AxiosInstance'

import BlogShortCard from '../../../components/HomePage/Blog/BlogShorts/BlogShortsCard'
import BlogCard from '../../../components/HomePage/Blog/BlogCard/BlogCard'

class BlogList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            queryData: [],
            url: 'blog/blog-viewset/'
        }
    }

    fetchData = () => {
        axiosInstance.get(this.state.url)
        .then(response => this.setState({
            queryData: response.data
        }))
        .catch(error => console.log(error))
    //     let data = [{
    //         id:1,
    //         title:'Test with dummy data',
    //         content:'Four Signs a Person Is Secretly Unhappy with Their Life',
    //         last_modified:'sunday',
    //         author:{
    //             fullname:'Amit'
    //         },
    //         img:blogimage1
    //     },
    //     {
    //         id:2,
    //         title:'Test with dummy data 2',
    //         content:'Do Not Use Print For Debugging In Python Anymore',
    //         last_modified:'monday',
    //         author:{
    //             fullname:'Alex'
    //         },
    //         img:blogimage2
    //     },
    //     {
    //         id:3,
    //         title:'Test with dummy data 3',
    //         content:'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsumlorem ipsum  lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsumlorem ipsum ',
    //         last_modified:'tuesday',
    //         author:{
    //             fullname:'Max'
    //         },
    //         img:blogimage3
    //     },
    //     {
    //         id:4,
    //         title:'Test with dummy data',
    //         content:'This is a dummy data to test the blog cards',
    //         last_modified:'sunday',
    //         author:{
    //             fullname:'Amit'
    //         },
    //         img:blogimage2
    //     },
    //     {
    //         id:5,
    //         title:'Test with dummy data 2',
    //         content:'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsumlorem ipsum ',
    //         last_modified:'monday',
    //         author:{
    //             fullname:'Alex'
    //         },
    //         img:blogimage1
    //     },
    //     {
    //         id:6,
    //         title:'Test with dummy data 3',
    //         content:'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsumlorem ipsum  lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsumlorem ipsum ',
    //         last_modified:'tuesday',
    //         author:{
    //             fullname:'Max'
    //         },
    //         img:blogimage3
    //     }
    // ]

    }

    componentDidMount() {
        this.fetchData()
    }
    render() {
        const { queryData } = this.state
        return (
            queryData.map(data=>{
                return(
                    this.props.loggedInUser==='' ?<BlogCard blogData={data} key={data.id}/>:<BlogShortCard blogData={data} key={data.id}/>
                )
            })
        )
    }
}

export default BlogList