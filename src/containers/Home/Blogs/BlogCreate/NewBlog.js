import React, { Component } from 'react'
import Dropzone from 'react-dropzone'
import { Button } from '@material-ui/core';

import './NewBlog.css'
import avtar from '../../../../assests/user.png'

class NewBlog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            file: {},
            showImage: false
        }
    }
    uploadImage = (acceptedFile) => 
        this.setState({
            file : Object.assign(acceptedFile,{preview:URL.createObjectURL(acceptedFile)}),
            showImage:true
        })
    removeImage = () => {
        this.setState({
            file: {},
            showImage: false
        })
    }
    BlogImageContainer = () => {
        const dropZone = <React.Fragment>
            <Dropzone onDrop={acceptedFile => this.uploadImage(acceptedFile[0])}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <div style={{ display: 'flex', justifyContent: 'center', cursor: 'pointer' }}><img alt='upload__document' src="https://img.icons8.com/pastel-glyph/64/000000/upload-document--v1.png" /></div>
                        <p style={{ opacity: '0.25' }}>Drag and drop your files here...</p>
                        <div style={{ display: 'flex', justifyContent: 'center' }}><p>OR</p></div>
                    </div>
                    
                )}
            </Dropzone>
            <input type='file' onChange={(e)=>this.uploadImage(e.target.files[0])}/>
        </React.Fragment>
        const previewImage =
            <React.Fragment>
                <div className='preview__box'>
                    <div className='image__preview'><img style={{ width: '100%', height: '100%' }} alt='preview__image' src={this.state.file.preview} key={this.state.file.name} /></div>
                    <div className='image__options'>
                        <img onClick={() => this.removeImage()} alt='cancel-icon' src="https://img.icons8.com/ios-glyphs/50/ffffff/cancel.png" />
                    </div>
                </div>
            </React.Fragment>
        if (this.state.showImage === true) {
            return (previewImage)
        } else {
            return (dropZone)
        }
    }
    render() {
        return (
            <React.Fragment>
                <div className='createmain__container'>
                    <div className='newblog__header'>
                        <div className='author__info'><img src={avtar} alt='' /><span>John Doe</span></div>
                        <div className='newblog__title'><input className='title__box' type='text' placeholder='Blog title name' /></div>
                        <div className='newblog__image'>
                            <this.BlogImageContainer />
                        </div>
                    </div>
                    <div className='newblog__body'>
                        <div className='newblog__content'>
                            <textarea className='content__box' type='textbox' placeholder='Start your thoughts here...' />
                        </div>
                    </div>
                    <div className='newblog__footer'>
                        <div className='newblog__options'>
                            <Button variant="contained" color="secondary">
                                Delete
                            </Button>
                            <Button variant="contained" color="primary">
                                Save
                            </Button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default NewBlog;