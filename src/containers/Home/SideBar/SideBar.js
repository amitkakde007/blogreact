import React,{Component} from 'react'

import SideBarUpdate,{SuggestionTags} from '../../../components/HomePage/SideBar/SideBar'

class SideBar extends Component{

    constructor(props){
        super(props);
        this.state={
            isUser:false
        }
    }
    render(){
        let sideBar = this.state.isUser===false? <SideBarUpdate/>: <React.Fragment><SuggestionTags/> <SideBarUpdate/></React.Fragment>
        return(
            <div className='side__bar' style={{display:'flex',flexDirection:'column'}}>
                {sideBar}
            </div>
        )
    }
}

export default SideBar