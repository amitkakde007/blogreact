import React,{useState,useContext} from 'react'

export const StateContext = React.createContext()
export const StateUpdateContext = React.createContext()

export function StateProvider({children}){

    const [loggedInUser,setLoggedInUser] = useState('')
    const setUser = (data)=>setLoggedInUser(data)
    return(
        <StateContext.Provider value={loggedInUser}>
            <StateUpdateContext.Provider value={setUser}>
                {children}  
            </StateUpdateContext.Provider>
        </StateContext.Provider>
    )
}

export const stateData = ()=> useContext(StateContext)
export const stateUpdate = ()=> useContext(StateUpdateContext)