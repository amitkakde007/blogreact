import axios from 'axios'
import Cookie  from 'universal-cookie'

const cookie = new Cookie()

const axiosInstance = axios.create({
    baseURL:'http://localhost:8000/',
    timeout:5000,
    headers:{
        'Content-type':'application/json',
        'accept':'application/json',
    },
    withCredentials:true
})

export const authAxios = axios.create({
    baseURL:'http://localhost:8000/',
    timeout:5000,
    headers:{
        'Content-type':'application/json',
        'accept':'application/json',
    },
})

axiosInstance.interceptors.response.use(
    response=>{
        authAxios.defaults.headers['Authorization'] = `JWT ${cookie.get('access')}`
        return response
    },)

authAxios.interceptors.response.use(
    response=>{
        return response
    },
    async error=>{
        const prevReq = error.config
        if (error.response.status===401 && error.response.statusText==='UnAuthorized' && error.config.url==='authenticate/api/token/refresh'){
            console.error(error)
        }
        else if (error.response.status===401 && error.response.statusText==='UnAuthorized' && error.response.data.code==='token_not_valid'){
            return axiosInstance.post('authenticate/api/token/refresh')
            .then(resp=>{ 
                if(resp.data==='Success'){
                    authAxios.defaults.headers['Authorization'] = `JWT ${cookie.get('access')}`
                    prevReq.headers['Authorization'] = `JWT ${cookie.get('access')}`
                    return axiosInstance(prevReq)
                }
                else console.log(resp)
            })
            .catch(err=>console.error(err))
        }
    }
)

export default axiosInstance;