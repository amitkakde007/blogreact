import React from 'react'
import {Route} from 'react-router-dom'
import { useHistory } from 'react-router';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';


function ProtectedRoute(props){

    const history = useHistory()
    return(
        <Route {...props} render={
            props=>{
                if (typeof(props.isUser)!=='undefined'){
                    console.log(props.isUser)
                    return <props.component/>
                }
                else{
                    return(
                    <Dialog open={true}>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                It seems you are not Logged In  
                                Please Sign In to continue...
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={()=>history.push('/')} color="secondary">
                                Don't want to sign in
                            </Button>
                            <Button onClick={()=>history.push('/login')} color="primary">
                                Continue to SignIn...
                            </Button>
                        </DialogActions>
                  </Dialog>
                    )
                }
            }
        }
        />
       
    )

}

export default ProtectedRoute;