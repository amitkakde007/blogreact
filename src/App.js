import React from 'react';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'

import Header from './containers/Home/Header/Header';
import HomePage from './containers/Home/HomePage';
import LoginModal from './containers/Modal/LoginModal';
import JumbotronPanel from './components/JumbotronPanel/JumbotronPanel';
import {stateData, StateProvider} from './StateProvider';
import ProtectedRoute from './ProtectedRoute';
import NewBlog from './containers/Home/Blogs/BlogCreate/NewBlog';
import BlogDetail from './containers/Home/Blogs/BlogDetail';

function App() {
  const loggedInUser = stateData()
  return (
    <Router>
      <StateProvider>
      <div className="App">
          <Switch>
            <Route path='/' exact>
              <Header/>
              <JumbotronPanel/>
              <HomePage/>
            </Route>
            <Route path='/login'>
              <LoginModal show={true}/>
            </Route>
            <Route path='/membership'>
              <Header/>
              <JumbotronPanel/>
              <HomePage/>
            </Route>
            <Route path='/about'>
              <Header/>
              <JumbotronPanel/>
              <HomePage/>
            </Route>
            <Route path='/home'>
              <Header/>
              <HomePage/>
            </Route> 
            <Route path='/blog/create/new'>
              <Header/>
              <NewBlog/>
            </Route>

            {/* <ProtectedRoute props={{
              path:'/home',
              component:<div><Header/><HomePage/></div>,
              isUser:loggedInUser}} />

              <ProtectedRoute props={{
                path:'/blog/create/new',
                Component:<div><Header/><NewBlog/></div>,
                isUser:loggedInUser}} /> */}

              <Route path='/blog/:id/'>
                <Header/>
                <BlogDetail/>
            </Route>
          </Switch>
      </div>
      </StateProvider>
    </Router>
  );
}

export default App;
